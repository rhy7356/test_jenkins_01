# Python 简单删除目录下文件以及文件夹
import os
import shutil
filelist=[]
# 选取删除文件夹的路径，最终结果删除report文件夹
rootdir=r'C:\Users\Administrator\Desktop\test_jenkins_01\report'
# 列出该目录下的所有文件名
filelist=os.listdir(rootdir)
for f in filelist:
    # 将文件名映射成绝对路径
    filepath=os.path.join(rootdir,f)
    # 判断该文件是否为文件或文件夹
    if os.path.isfile(filepath):
        # 若为文件则删除
        os.remove(filepath)
        print(str(filepath)+' '+"removed!")
    #     若为文件夹，则删除该文件夹及文件夹内所有文件
    elif os.path.isdir(filepath):
        shutil.rmtree(filepath,True)
        print("dir"+str(filepath)+' '+"removed!")
#         最后删除总文件夹report
shutil.rmtree(rootdir,True)
print("删除成功")
import unittest
from HTMLTestRunner import HTMLTestRunner
import time
discov=unittest.defaultTestLoader.discover("./",pattern='test_*.py')
if __name__=='__main__':
    report_dir="./report"
    now=time.strftime("%Y-%m-%d %H_%M_%S")
    report_file=report_dir+'/'+now+"_report.html"
    with open(report_file,'wb') as f:
        runn=HTMLTestRunner(stream=f,title='testReport',description='test_report')
        runn.run(discov)
    f.close()
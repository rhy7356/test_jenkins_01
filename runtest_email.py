import unittest
from HTMLTestRunner import HTMLTestRunner
import time
import smtplib
from email.mime.text import MIMEText
from email.header import Header
import os
def send_mail(latest_report):
    f=open(latest_report,'rb')
    mail_content=f.read()
    f.close()
    smtpserver='smtp.qq.com'
    user='511928867@qq.com'
    password='ucrwdgeabejpcacj'
    sender='511928867@qq.com'
    receives=['511928867@qq.com','rhy_18801139192@163.com']
    subject='自动化测试报告'
    msg=MIMEText(mail_content,'html','utf-8')
    msg['Subject']=Header(subject,'utf-8')
    msg['From']=sender
    msg['To']=','.join(receives)
    smtp=smtplib.SMTP_SSL(smtpserver,465)
    smtp.helo(smtpserver)
    smtp.ehlo(smtpserver)
    smtp.login(user,password)
    print("Start send email...")
    smtp.sendmail(sender,receives,msg.as_string())
    smtp.quit()
    print("Send email end!")
def latest_report(report_dir):
    lists = os.listdir(report_dir)
    print(lists)
    lists.sort(key=lambda fn: os.path.getatime(report_dir + '\\' + fn))
    print("the latest report is"+lists[-1])
    # 最新的报告文件路径
    file = os.path.join(report_dir, lists[-1])
    print(file)
    return file

if __name__=='__main__':
    discov = unittest.defaultTestLoader.discover("./", pattern='test_*.py')
    report_dir="./report"
    now=time.strftime("%Y-%m-%d %H_%M_%S")
    report_file=report_dir+'/'+now+"_report.html"
    with open(report_file,'wb') as f:
        runn=HTMLTestRunner(stream=f,title='Test Report',description='test_report')
        runn.run(discov)
    f.close()
    latest_report=latest_report(report_dir)
    send_mail(latest_report)

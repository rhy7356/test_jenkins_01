import unittest
import csv
class TestFile(unittest.TestCase):
    def test_file(self):
        with open(r'C:\\Users\\Administrator\\Desktop\\testing\\file\\people.csv','r') as f:
            self.file_data=csv.reader(f)
            # print(self.file_data) csv.reader返回的是一个迭代类型，索引应该用循环来打印信息才行
            for peo in self.file_data:
                print(peo)
                print(peo[0])
    def test_write(self):
        self.stu0=['laoliu000',19,'wuhan']
        self.stu1=['liqi000',18,'shandong']
        # 打开文件,a追加，w直接覆盖
        with open(r'C:\\Users\\Administrator\\Desktop\\testing\\file\\people.csv','a',newline='') as fp:
            # 设定写入模式
            self.csv_write=csv.writer(fp,dialect='excel')
            # 写入具体内容
            try:
                self.csv_write.writerow(self.stu0)
                self.csv_write.writerow(self.stu1)
                print("write file over!")
            except Exception as e:
                raise e
    def test_newfile(self):
        with open(r'C:\\Users\\Administrator\\Desktop\\testing\\file\\students.txt', 'r') as f0:
            lines = f0.readlines()
            print(lines)
        with open(r'C:\\Users\\Administrator\\Desktop\\test_jenkins_01\\file\\new3.txt', 'w') as f1:
            try:
                for line in lines:
                    f1.write(line)
                print("write success!")
            except Exception as e:
                raise e
if __name__=='__main__':
    unittest.main()

